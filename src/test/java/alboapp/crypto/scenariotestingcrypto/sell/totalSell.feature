Feature: TOTAL SELL CRYPTO
Background: Setup
    * def pattern = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'"
    * def getDate =
                    """
                    function() {
                      var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
                      var sdf = new SimpleDateFormat(pattern);
                      var date = new java.util.Date();
                      return sdf.format(date);
                    }
                    """
    * def getAmount =
                    """
                    function() {
                      var max = 110
                      var min = 49;
                      numb = Math.random() * (max - min + 1) + min;
                      return parseFloat(numb).toFixed(2);
                      }
                    """

    * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
    * def pause = karate.get('__gatling.pause', sleep)
###
### BITCOIN ###
###
#CatalogList
Scenario: Get Crypto Currencies
        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'CatalogCryptoCurrencies'
        And   url baseUrlCatCryCurrencies
        And   def consulta = 'currencies'
        And   def today = getDate()

        When  request
            """
                    {
                    "data": {
                        "catalog": {
                        "id": '#(consulta)'
                        }
                    }
                    }
            """
        And   method post
        Then  status 200
        And   def BTC = $.data.catalog.items[0].m[0].value
        And   def ETH = $.data.catalog.items[2].m[0].value
        And   def COMP = $.data.catalog.items[1].m[0].value
#TransferCreate
Scenario: Transfer Create - Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'TransferCreateCrypto'
            And   url baseUrlTransferCreateCrypto
            And   def amountTest = getAmount()
            And   string amount = amountTest
            And   def datosTest = __gatling.TCreateCryp
            And   def entity_uuid = get[0] datosTest
            And   def parent_uuid = get[1] datosTest
            And   def source_space_uuid = parent_uuid
            And   def target_space_uuid = get[2] datosTest
            And   def target_space_currency = get[3] datosTest
            When  request
                """

                """
            And   method post
            Then  status 200
##MoneyOutRequest
Scenario: MoneyOut Sell transfer Crypto
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'MoneyOutCrypto'
            And   url baseUrlMoneyOut
            And   def target_receipt_uuid = get[4] datosTest

            When  request
                    """
                            {
                            "data": {
                                "money": {
                                "value": {
                                    "amount": '#(~~amountTest)',
                                    "concept": "operation",
                                    "reference": "00001",
                                    "source_customer_uuid": '#(entity_uuid)',
                                    "source_space_uuid": '#(parent_uuid)',
                                    "target_receipt_uuid": '#(target_receipt_uuid)',
                                    "transaction_uuid": '#(transaction_uuid)'
                                }
                                }
                            }
                            }

                    """
            And   method post
            Then  status 200
#### Wait callback
* pause(7000)
#SpaceListTX
Scenario: Check Crypto Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'SpaceListTxCrypto'
            And   url baseUrlSpaceListTxCrypto
            When  request
                    """
                        {
                            "data": {
                                "space": {
                                    "uuid": '#(target_space_uuid)'
                                },
                                "filter": {
                                    "results_per_page": 300,
                                    "page_number": 1
                                }
                            }
                        }

                    """
            And   method post
            Then  status 200
###
### Ethereum ###
###
#CatalogList
Scenario: Get Crypto Currencies
        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'CatalogCryptoCurrencies'
        And   url baseUrlCatCryCurrencies
        And   def consulta = 'currencies'
        And   def today = getDate()

        When  request
            """
                    {
                    "data": {
                        "catalog": {
                        "id": '#(consulta)'
                        }
                    }
                    }
            """
        And   method post
        Then  status 200
        And   def BTC = $.data.catalog.items[0].m[0].value
        And   def ETH = $.data.catalog.items[2].m[0].value
        And   def COMP = $.data.catalog.items[1].m[0].value
#TransferCreate
Scenario: Transfer Create - Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'TransferCreateCrypto'
            And   url baseUrlTransferCreateCrypto
            And   def amountTest = getAmount()
            And   string amount = amountTest
            And   def datosTest = __gatling.TCreateCryp
            And   def entity_uuid = get[0] datosTest
            And   def parent_uuid = get[1] datosTest
            And   def source_space_uuid = parent_uuid
            And   def target_space_uuid = get[2] datosTest
            And   def target_space_currency = get[3] datosTest
            When  request
                """

                """
            And   method post
            Then  status 200
##MoneyOutRequest
Scenario: MoneyOut Sell transfer Crypto
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'MoneyOutCrypto'
            And   url baseUrlMoneyOut
            And   def target_receipt_uuid = get[4] datosTest

            When  request
                    """
                            {
                            "data": {
                                "money": {
                                "value": {
                                    "amount": '#(~~amountTest)',
                                    "concept": "operation",
                                    "reference": "00001",
                                    "source_customer_uuid": '#(entity_uuid)',
                                    "source_space_uuid": '#(parent_uuid)',
                                    "target_receipt_uuid": '#(target_receipt_uuid)',
                                    "transaction_uuid": '#(transaction_uuid)'
                                }
                                }
                            }
                            }

                    """
            And   method post
            Then  status 200
#### Wait callback
* pause(7000)
#SpaceListTX
Scenario: Check Crypto Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'SpaceListTxCrypto'
            And   url baseUrlSpaceListTxCrypto
            When  request
                    """
                        {
                            "data": {
                                "space": {
                                    "uuid": '#(target_space_uuid)'
                                },
                                "filter": {
                                    "results_per_page": 300,
                                    "page_number": 1
                                }
                            }
                        }

                    """
            And   method post
            Then  status 200
###
### Compound ###
###
#CatalogList
Scenario: Get Crypto Currencies
        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'CatalogCryptoCurrencies'
        And   url baseUrlCatCryCurrencies
        And   def consulta = 'currencies'
        And   def today = getDate()

        When  request
            """
                    {
                    "data": {
                        "catalog": {
                        "id": '#(consulta)'
                        }
                    }
                    }
            """
        And   method post
        Then  status 200
        And   def BTC = $.data.catalog.items[0].m[0].value
        And   def ETH = $.data.catalog.items[2].m[0].value
        And   def COMP = $.data.catalog.items[1].m[0].value
#TransferCreate
Scenario: Transfer Create - Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'TransferCreateCrypto'
            And   url baseUrlTransferCreateCrypto
            And   def amountTest = getAmount()
            And   string amount = amountTest
            And   def datosTest = __gatling.TCreateCryp
            And   def entity_uuid = get[0] datosTest
            And   def parent_uuid = get[1] datosTest
            And   def source_space_uuid = parent_uuid
            And   def target_space_uuid = get[2] datosTest
            And   def target_space_currency = get[3] datosTest
            When  request
                """

                """
            And   method post
            Then  status 200
##MoneyOutRequest
Scenario: MoneyOut Sell transfer Crypto
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'MoneyOutCrypto'
            And   url baseUrlMoneyOut
            And   def target_receipt_uuid = get[4] datosTest

            When  request
                    """
                            {
                            "data": {
                                "money": {
                                "value": {
                                    "amount": '#(~~amountTest)',
                                    "concept": "operation",
                                    "reference": "00001",
                                    "source_customer_uuid": '#(entity_uuid)',
                                    "source_space_uuid": '#(parent_uuid)',
                                    "target_receipt_uuid": '#(target_receipt_uuid)',
                                    "transaction_uuid": '#(transaction_uuid)'
                                }
                                }
                            }
                            }

                    """
            And   method post
            Then  status 200
#### Wait callback
* pause(7000)
#SpaceListTX
Scenario: Check Crypto Sell
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'SpaceListTxCrypto'
            And   url baseUrlSpaceListTxCrypto
            When  request
                    """
                        {
                            "data": {
                                "space": {
                                    "uuid": '#(target_space_uuid)'
                                },
                                "filter": {
                                    "results_per_page": 300,
                                    "page_number": 1
                                }
                            }
                        }

                    """
            And   method post
            Then  status 200