package alboapp.crypto.scenariotestingcrypto;

import com.intuit.karate.junit5.Karate;

public class scenarioTestingRunner {


    //Run all Scenarios
    @Karate.Test
    Karate testSample() {
        return Karate.run("scenariotestingcrypto").relativeTo(getClass());
    }
    
    //Run only test with tags
    // @Karate.Test
    // Karate testTags() {
    //     return Karate.run("scenariotestingcrypto").tags("@first").relativeTo(getClass());
    // }

    //Run test with tags and set properties
    // @Karate.Test
    // Karate testSystemProperty() {
    //     return Karate.run("classpath:alboApp/crypto/scenariotestingcrypto/spacelistcrypto.feature")
    //             .tags("@second")
    //             .karateEnv("sbx");
    //             // .systemProperty("foo", "bar")
    // } 
    
    
}
