Feature: BUY CRYPTO

Background: Setup
    * def pattern = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'"
    * def getDate =
                    """
                    function() {
                      var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
                      var sdf = new SimpleDateFormat(pattern);
                      var date = new java.util.Date();
                      return sdf.format(date);
                    }
                    """
    * def getAmount =
                    """
                    function() {
                      var max = 110
                      var min = 49;
                      numb = Math.random() * (max - min + 1) + min;
                      return parseFloat(numb).toFixed(2);
                      }
                    """

    * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
    * def pause = karate.get('__gatling.pause', sleep)
### BITCOIN ###
#CatalogList
Scenario: Get Crypto Currencies
        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'CatalogCryptoCurrencies'
        And   url baseUrlCatCryCurrencies
        And   def consulta = 'currencies'
        When  request
            """
                    {
                    "data": {
                        "catalog": {
                        "id": '#(consulta)'
                        }
                    }
                    }
            """
        And   method post
        Then  status 200
        And   def BTC = $.data.catalog.items[0].m[0].buy_value
        And   def ETH = $.data.catalog.items[2].m[0].buy_value
        And   def COMP = $.data.catalog.items[1].m[0].buy_value
#TransferCreate
Scenario: Transfer Create - Buy
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'TransferCreateCrypto'
            And   url baseUrlTransferCreateCrypto
            And   def amountTest = getAmount()
            And   string amount = amountTest
            # And   def datosTest = __gatling.TCreateCryp
            # And   def entity_uuid = get[0] datosTest
            # And   def parent_uuid = get[1] datosTest
            And   def entity_uuid = "16262811415260a196a4a-5eb4-4bd4-8206-607c62b07355"
            And   def parent_uuid = "os-space-1626294356284e29682e6e3144d25ad5a3e052f69339b"
            And   def source_space_uuid = parent_uuid
            # And   def target_space_uuid = get[2] datosTest
            # And   def target_space_currency = get[3] datosTest
            And   def target_space_uuid = "scWallet-1626392558932da549f4333d94b01ab1fc9883241ccac"
            # And   def target_space_currency = "BTC"
            When  request
                """
                    {
                    "data": {
                        "amount": "55.55",
                        "concept": "crypto operation",
                        "owner": {
                        "application_uuid": "1",
                        "entity_uuid": '#(entity_uuid)',
                        "parent_uuid": '#(parent_uuid)'
                        },
                        "reference": "crypto",
                        "source": {
                        "space_currency": "MXN",
                        "space_type": "debit",
                        "space_uuid": '#(source_space_uuid)'
                        },
                        "target": {
                        "space_currency": "BTC",
                        "space_type": "crypto",
                        "space_uuid": '#(target_space_uuid)'
                        }
                    }
                    }
                """
            And   method post
            Then  status 200
            And match response ==
            """
                    {
                    "data": {
                        "amount": '#notnull',
                        "currency": "BTC",
                        "due_date": '#notnull',
                        "exchange_rate": '#notnull',
                        "transaction_uuid": '#notnull'
                    },
                    "response": "OK"
                    }
            """
            And   def amount = $.data.amount
            And   def exRate = $.data.exchange_rate
            And   def dueDate = $.data.due_date
            And   def trans_uuid = $.data.transaction_uuid
##MoneyOutRequest
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'MoneyOutCrypto'
            And   url baseUrlMoneyOut
            When  request
                    """
                    {
                        "data": {
                            "money": {
                            "value": {
                                "amount": 55.55,
                                "concept": "operation",
                                "reference": "00001",
                                "source_customer_uuid": '#(entity_uuid)',
                                "source_space_uuid": '#(parent_uuid)',
                                "target_receipt_uuid": "162639256038820fb0e62-22ec-4473-9ce9-2facb530f59a",
                                "transaction_uuid": '#(trans_uuid)'
                            }
                            }
                         }
                     }
                   """
            And   method post
            Then  status 200
            And match response ==
                        """
                        {
                                "data": {
                                "authorization": '#notnull',
                                "details": {
                                "moneyout_process": '#notnull'
                                },
                                "status": "OK"
                            },
                            "response": "OK"
                        }
                        """
            And   def authorization = $.data.authorization
            And   def moneyout_process = $.data.moneyout_process
#### Wait callback
* pause(9000)
#SpaceListTX
            Given header Content-Type = 'application/json'
            And   header Authorization = authorizationTest
            And   header Token = tokenTest
            And   header karate-name = 'SpaceListTxCrypto'
            And   url baseUrlSpaceListTxCrypto
            When  request
                    """
                        {
                            "data": {
                                "space": {
                                    "uuid": '#(target_space_uuid)'
                                },
                                "filter": {
                                    "results_per_page": 300,
                                    "page_number": 1
                                }
                            }
                        }

                    """
            And   method post
            Then  status 200
            And   match response contains '56.11'
# ### Ethereum ###
# #CatalogList
# Scenario: Get Crypto Currencies
#         Given header Content-Type = 'application/json'
#         And   header Authorization = authorizationTest
#         And   header Token = tokenTest
#         And   header karate-name = 'CatalogCryptoCurrencies'
#         And   url baseUrlCatCryCurrencies
#         And   def consulta = 'currencies'
#         And   def today = getDate()

#         When  request
#             """
#                     {
#                     "data": {
#                         "catalog": {
#                         "id": '#(consulta)'
#                         }
#                     }
#                     }
#             """
#         And   method post
#         Then  status 200
#         And   def BTC = $.data.catalog.items[0].m[0].value
#         And   def ETH = $.data.catalog.items[2].m[0].value
#         And   def COMP = $.data.catalog.items[1].m[0].value
# #TransferCreate
# Scenario: Transfer Create - Buy
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'TransferCreateCrypto'
#             And   url baseUrlTransferCreateCrypto
#             And   def amountTest = getAmount()
#             And   string amount = amountTest
#             And   def datosTest = __gatling.TCreateCryp
#             And   def entity_uuid = get[0] datosTest
#             And   def parent_uuid = get[1] datosTest
#             And   def source_space_uuid = parent_uuid
#             And   def target_space_uuid = get[2] datosTest
#             And   def target_space_currency = get[3] datosTest
#             When  request
#                 """

#                 """
#             And   method post
#             Then  status 200
# ##MoneyOutRequest
# Scenario: MoneyOut Buy transfer Crypto
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'MoneyOutCrypto'
#             And   url baseUrlMoneyOut
#             And   def target_receipt_uuid = get[4] datosTest

#             When  request
#                     """
#                             {
#                             "data": {
#                                 "money": {
#                                 "value": {
#                                     "amount": '#(~~amountTest)',
#                                     "concept": "operation",
#                                     "reference": "00001",
#                                     "source_customer_uuid": '#(entity_uuid)',
#                                     "source_space_uuid": '#(parent_uuid)',
#                                     "target_receipt_uuid": '#(target_receipt_uuid)',
#                                     "transaction_uuid": '#(transaction_uuid)'
#                                 }
#                                 }
#                             }
#                             }

#                     """
#             And   method post
#             Then  status 200
# #### Wait callback
# * pause(7000)
# #SpaceListTX
# Scenario: Check Crypto Buy
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'SpaceListTxCrypto'
#             And   url baseUrlSpaceListTxCrypto
#             When  request
#                     """
#                         {
#                             "data": {
#                                 "space": {
#                                     "uuid": '#(target_space_uuid)'
#                                 },
#                                 "filter": {
#                                     "results_per_page": 300,
#                                     "page_number": 1
#                                 }
#                             }
#                         }

#                     """
#             And   method post
#             Then  status 200
# ### Compound ###
# #CatalogList
# Scenario: Get Crypto Currencies
#         Given header Content-Type = 'application/json'
#         And   header Authorization = authorizationTest
#         And   header Token = tokenTest
#         And   header karate-name = 'CatalogCryptoCurrencies'
#         And   url baseUrlCatCryCurrencies
#         And   def consulta = 'currencies'
#         And   def today = getDate()

#         When  request
#             """
#                     {
#                     "data": {
#                         "catalog": {
#                         "id": '#(consulta)'
#                         }
#                     }
#                     }
#             """
#         And   method post
#         Then  status 200
#         And   def BTC = $.data.catalog.items[0].m[0].value
#         And   def ETH = $.data.catalog.items[2].m[0].value
#         And   def COMP = $.data.catalog.items[1].m[0].value
# #TransferCreate
# Scenario: Transfer Create - Buy
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'TransferCreateCrypto'
#             And   url baseUrlTransferCreateCrypto
#             And   def amountTest = getAmount()
#             And   string amount = amountTest
#             And   def datosTest = __gatling.TCreateCryp
#             And   def entity_uuid = get[0] datosTest
#             And   def parent_uuid = get[1] datosTest
#             And   def source_space_uuid = parent_uuid
#             And   def target_space_uuid = get[2] datosTest
#             And   def target_space_currency = get[3] datosTest
#             When  request
#                 """

#                 """
#             And   method post
#             Then  status 200
# ##MoneyOutRequest
# Scenario: MoneyOut Buy transfer Crypto
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'MoneyOutCrypto'
#             And   url baseUrlMoneyOut
#             And   def target_receipt_uuid = get[4] datosTest

#             When  request
#                     """
#                             {
#                             "data": {
#                                 "money": {
#                                 "value": {
#                                     "amount": '#(~~amountTest)',
#                                     "concept": "operation",
#                                     "reference": "00001",
#                                     "source_customer_uuid": '#(entity_uuid)',
#                                     "source_space_uuid": '#(parent_uuid)',
#                                     "target_receipt_uuid": '#(target_receipt_uuid)',
#                                     "transaction_uuid": '#(transaction_uuid)'
#                                 }
#                                 }
#                             }
#                             }

#                     """
#             And   method post
#             Then  status 200
# #### Wait callback
# * pause(7000)
# #SpaceListTX
# Scenario: Check Crypto Buy
#             Given header Content-Type = 'application/json'
#             And   header Authorization = authorizationTest
#             And   header Token = tokenTest
#             And   header karate-name = 'SpaceListTxCrypto'
#             And   url baseUrlSpaceListTxCrypto
#             When  request
#                     """
#                         {
#                             "data": {
#                                 "space": {
#                                     "uuid": '#(target_space_uuid)'
#                                 },
#                                 "filter": {
#                                     "results_per_page": 300,
#                                     "page_number": 1
#                                 }
#                             }
#                         }

#                     """
#             And   method post
#             Then  status 200