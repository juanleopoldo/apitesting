package alboapp.crypto.scenariotestingcrypto.buy;

import com.intuit.karate.junit5.Karate;

public class buyCryptoRunner {


    // //Run all Scenarios
    // @Karate.Test
    // Karate testSample() {
    //     return Karate.run("alboapp/crypto/scenariotestingcrypto/buy/buyCrypto").relativeTo(getClass());
    // }
    
    // //Run only test with tags
    // // @Karate.Test
    // // Karate testTags() {
    // //     return Karate.run("buyCrypto").tags("@first").relativeTo(getClass());
    // // }

    //Run test with tags and set properties
    @Karate.Test
    Karate testSystemProperty() {
        return Karate.run("classpath:alboapp/crypto/scenariotestingcrypto/buy/buyCrypto.feature")
                // .tags("@second")
                .karateEnv("sbx");
                // .systemProperty("foo", "bar")
    } 
    
    
}
