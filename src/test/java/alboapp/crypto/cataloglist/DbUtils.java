package alboapp.crypto.cataloglist;

// import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;

import com.fasterxml.jackson.databind.ObjectMapper;
// import com.intuit.karate.Json;
// import com.mongodb.BasicDBObject;
// import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

// import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mongodb.MongoClientURI;


public class DbUtils {

    private static final Logger logger = LoggerFactory.getLogger(DbUtils.class);

    private MongoClient mongoClient;

  public DbUtils(Map<String, Object> config) {

        String host = (String) config.get("host");
        int port = (int) config.get("port");  
        MongoClientURI uri = new MongoClientURI("mongodb://qa_user_leopoldoenc:8jbxfjq1EO7rGNyG@"+host+":"+port+"/admin?ssl=true&retryWrites=true&w=majority&authMechanism=SCRAM-SHA-1&tlsAllowInvalidHostnames=true");
        mongoClient = new MongoClient(uri);
        System.out.println("Inicia.Conexión.Mongo");
  }

  

  public void insertValues(String database, String collection, List<Object> query) {

    List<Document> documents = new ArrayList<>();
    ObjectMapper objectMapper = new ObjectMapper();

    try {
      for (Object object : query) {
        String documentString = objectMapper.writeValueAsString(object);
        Document doc = Document.parse(documentString);
        documents.add(doc);
      }

    } catch (IOException e) {
      logger.error("Error in parsing document for collection");
    }

    mongoClient.getDatabase(database).getCollection(collection).insertMany(documents);

  }

  public void deleteDocuments(String database, String collection, String field,
      Iterable<String> ids) {
    mongoClient.getDatabase(database).getCollection(collection).deleteMany(in(field, ids));
  }
  
  public String getId(String database, String collection, String field, String value) {
    System.out.println("Inicia.Mongo.getId");
    
    MongoDatabase db = mongoClient.getDatabase(database);
    MongoCollection<Document> coll = db.getCollection(collection);
    FindIterable<Document> doc = coll.find();

    // String objectdb = "";

    for (Document document : doc) {
      // String id = ((ObjectId) document.get("_id")).toString();

      // objectdb = ((ObjectId) document.get("value")).toString();
      // BasicDBObject doc = new BasicDBObject( "name", "Matt" );
      // collection.insert( doc );
      // ObjectId id = (ObjectId)doc.get( "_id" );


      System.out.println(document);

    }
    // System.out.println("El Resutado es:" + id);
    System.out.println("Finaliza.Mongo.getId");
    
    return null;


    
  }



  public void disconnect() {
    System.out.println("Finaliza.Conexión.Mongo");
    mongoClient.close();
  }


}