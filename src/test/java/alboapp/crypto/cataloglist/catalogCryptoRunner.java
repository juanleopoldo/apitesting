package alboapp.crypto.cataloglist;

import com.intuit.karate.junit5.Karate;

public class catalogCryptoRunner {


    // //Run all Scenarios
    // @Karate.Test
    // Karate testSample() {
    //     return Karate.run("catalogCurrencies").relativeTo(getClass());
    // }
    
    //Run only test with tags
    // @Karate.Test
    // Karate testTags() {
    //     return Karate.run("catalogCurrencies").tags("@contract").relativeTo(getClass());
    // }

    // //Run test with tags and set properties
    @Karate.Test
    Karate testSystemProperty() {
         return Karate.run("classpath:alboapp/crypto/cataloglist/catalogCurrencies.feature")
        //         .tags("@data")
                .karateEnv("sbx");
                // .systemProperty("foo", "bar")
    } 
    
    
}
