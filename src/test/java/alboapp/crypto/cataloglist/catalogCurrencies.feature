Feature: CATALOG GET  CURRENCIES

Background: Setup
          * def pattern = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'"
          * def getDate =
            """
            function() {
              var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
              var sdf = new SimpleDateFormat(pattern);
              var date = new java.util.Date();
              return sdf.format(date);
            }
            """
          * def config = { host: '127.0.0.1', port: 27021 }
          * def DbUtils = Java.type('alboapp.crypto.cataloglist.DbUtils')
          * def db = new DbUtils(config)
          * configure afterFeature =
          """
            function(){
              db.disconnect()
            }
          """
###
###Contract Testing
###
@contract
Scenario: Structure validation in the body of the request and the response
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          When  request
          """
            {
              "data": {
                "catalog": {
                  "id": '#(consulta)'
                }
              }
            }
          """
          And   method post
          Then  status 200
          And match response == 
          """
            {
                "data": {
                    "catalog": {
                        "items": [
                            {
                                "k": '#present',
                                "m": [
                                    {
                                        "buy_fee": '#present',
                                        "buy_max_value": '#present',
                                        "buy_min_value": '#present',
                                        "buy_value": '#present',
                                        "icon": '#present',
                                        "name": '#present',
                                        "priority": '#present',
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": '#present',
                                                "sell_fee": '#present'
                                            }
                                        },
                                        "sell_fee": '#present',
                                        "sell_max_value": '#present',
                                        "sell_min_value": '#present',
                                        "sell_value": '#present',
                                        "type": '#present',
                                        "value": '#present'
                                    }
                                ],
                                "v": '#present'
                            },
                            {
                                "k": '#present',
                                "m": [
                                    {
                                        "buy_fee": '#present',
                                        "buy_max_value": '#present',
                                        "buy_min_value": '#present',
                                        "buy_value": '#present',
                                        "icon": '#present',
                                        "name": '#present',
                                        "priority": '#present',
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": '#present',
                                                "sell_fee": '#present'
                                            }
                                        },
                                        "sell_fee": '#present',
                                        "sell_max_value": '#present',
                                        "sell_min_value": '#present',
                                        "sell_value": '#present',
                                        "type": '#present',
                                        "value": '#present'
                                    }
                                ],
                                "v": '#present'
                            },
                            {
                                "k": '#present',
                                "m": [
                                    {
                                        "buy_fee": '#present',
                                        "buy_max_value": '#present',
                                        "buy_min_value": '#present',
                                        "buy_value": '#present',
                                        "icon": '#present',
                                        "name": '#present',
                                        "priority": '#present',
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": '#present',
                                                "sell_fee": '#present'
                                            }
                                        },
                                        "sell_fee": '#present',
                                        "sell_max_value": '#present',
                                        "sell_min_value": '#present',
                                        "sell_value": '#present',
                                        "type": '#present',
                                        "value": '#present'
                                    }
                                ],
                                "v": '#present'
                            }
                        ]
                    }
                },
                "response": '#present'
            }
          """
          And match consulta == '#present'
@contract
Scenario: Validation of the value of each field in the response body
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          When  request
          """
            {
              "data": {
                "catalog": {
                  "id": '#(consulta)'
                }
              }
            }
          """
          And   method post
          Then  status 200
          And match response == 
          """
            {
                "data": {
                    "catalog": {
                        "items": [
                            {
                                "k": "BTC",
                                "m": [
                                    {
                                        "buy_fee": "1.00",
                                        "buy_max_value": "100000.00",
                                        "buy_min_value": "50.00",
                                        "buy_value": '#string? _ > 600000',
                                        "icon": "https://storage.googleapis.com/public.sbx-albo.tech/apps-resources/crypto-currencies/Bitcoin%403x.png",
                                        "name": "Bitcoin",
                                        "priority": "0",
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": "0.75",
                                                "sell_fee": "0.75"
                                            }
                                        },
                                        "sell_fee": "1.00",
                                        "sell_max_value": "100000.00",
                                        "sell_min_value": "50.00",
                                        "sell_value": '#string? _ > 600000',
                                        "type": "7",
                                        "value": '#string? _ > 600000'
                                    }
                                ],
                                "v": "BTC"
                            },
                            {
                                "k": "COMP",
                                "m": [
                                    {
                                        "buy_fee": "1.00",
                                        "buy_max_value": "100000.00",
                                        "buy_min_value": "50.00",
                                        "buy_value": '#string? _ > 0',
                                        "icon": "https://storage.googleapis.com/public.sbx-albo.tech/apps-resources/crypto-currencies/Compound%403x.png",
                                        "name": "Compound",
                                        "priority": "2",
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": "0.75",
                                                "sell_fee": "0.75"
                                            }
                                        },
                                        "sell_fee": "1.00",
                                        "sell_max_value": "100000.00",
                                        "sell_min_value": "50.00",
                                        "sell_value": '#string? _ > 0',
                                        "type": "7",
                                        "value": '#string? _ > 0'
                                    }
                                ],
                                "v": "COMP"
                            },
                            {
                                "k": "ETH",
                                "m": [
                                    {
                                        "buy_fee": "1.00",
                                        "buy_max_value": "100000.00",
                                        "buy_min_value": "50.00",
                                        "buy_value": '#string? _ > 0',
                                        "icon": "https://storage.googleapis.com/public.sbx-albo.tech/apps-resources/crypto-currencies/Ethereum%403x.png",
                                        "name": "Ethereum",
                                        "priority": "1",
                                        "provider_fee": {
                                            "wyre": {
                                                "buy_fee": "0.75",
                                                "sell_fee": "0.75"
                                            }
                                        },
                                        "sell_fee": "1.00",
                                        "sell_max_value": "100000.00",
                                        "sell_min_value": "50.00",
                                        "sell_value": '#string? _ > 0',
                                        "type": "7",
                                        "value": '#string? _ > 0'
                                    }
                                ],
                                "v": "ETH"
                            }
                        ]
                    }
                },
                "response": "OK"
            }
          """
          And match consulta == 'currencies'
@contract
Scenario: Field type validation in the body of the request and the response
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          And def req =
          """
            {
              "data": {
                "catalog": {
                    "id": '#(consulta)'
                }
              }
            }
          """
          When  request
          """
            {
              "data": {
                "catalog": {
                    "id": '#(consulta)'
                }
              }
            }
          """
          And  method post
          Then status 200
          And match req == 
          """
            {
              "data": {
                "catalog": '#object'
              }
            }
          """
          And match req == 
          """
            {
              "data": '#object'
            }
          """
          And match req == '#object'
          And match response == 
          """
            {
              "data": {
                  "catalog": {
                        "items": [
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": {
                                            "wyre": '#object'
                                        },
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": {
                                            "wyre": '#object'
                                        },
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": {
                                            "wyre": '#object'
                                        },
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            }
                        ]
                    }
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": {
                  "catalog": {
                        "items": [
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": '#object',
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": '#object',
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    {
                                        "buy_fee": '#string',
                                        "buy_max_value": '#string',
                                        "buy_min_value": '#string',
                                        "buy_value": '#string',
                                        "icon": '#string',
                                        "name": '#string',
                                        "priority": '#string',
                                        "provider_fee": '#object',
                                        "sell_fee": '#string',
                                        "sell_max_value": '#string',
                                        "sell_min_value": '#string',
                                        "sell_value": '#string',
                                        "type": '#string',
                                        "value": '#string'
                                    }
                                ],
                                "v": '#string'
                            }
                        ]
                    }
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": {
                  "catalog": {
                        "items": [
                            {
                                "k": '#string',
                                "m": [
                                    '#object'
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    '#object'
                                ],
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": [
                                    '#object'
                                ],
                                "v": '#string'
                            }
                        ]
                    }
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": {
                  "catalog": {
                        "items": [
                            {
                                "k": '#string',
                                "m": '#[1]',
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": '#[1]',
                                "v": '#string'
                            },
                            {
                                "k": '#string',
                                "m": '#[1]',
                                "v": '#string'
                            }
                        ]
                    }
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": {
                  "catalog": {
                        "items": '#[3]'
                    }
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": {
                  "catalog": '#object'
                },
                "response": '#string'
            }
          """
          And match response == 
          """
            {
              "data": '#object',
                "response": '#string'
            }
          """
          And match response == '#object'
          And match consulta == '#string'
###
###Component Testing
###
@exceptions
Scenario: id_does_not_exist_in_database
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies2'
          When  request
          """
                      {
                        "data": {
                          "catalog": {
                            "id": '#(consulta)'
                          }
                        }
                      }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                    {
                        "data": {
                            "exception": {
                                "code": "0027.000.003",
                                "detail": "data.catalog::id_does_not_exist_in_database"
                            }
                        },
                        "response": "BAD"
                    }
          """
@exceptions
Scenario: id not string
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          And   def consulta = 666
          When  request
          """
                      {
                        "data": {
                          "catalog": {
                            "id": '#(consulta)'
                          }
                        }
                      }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                  {
                      "data": {
                          "exception": {
                              "code": "0027.000.001",
                              "detail": "data.catalog::id_not_found"
                          }
                      },
                      "response": "BAD"
                  }
          """
@exceptions
Scenario: id_can_not_be_empty 
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          When  request
          """
                      {
                        "data": {
                          "catalog": {
                            "id": ""
                          }
                        }
                      }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                  {
                      "data": {
                          "exception": {
                              "code": "0027.000.002",
                              "detail": "data.catalog::id_can_not_be_empty"
                          }
                      },
                      "response": "BAD"
                  }
          """
@exceptions
Scenario: Different name in variable id
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          When  request
          """
                      {
                        "data": {
                          "catalog": {
                            "idX": '#(consulta)'
                          }
                        }
                      }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                    {
                        "data": {
                            "exception": {
                                "code": "0027.000.001",
                                "detail": "data.catalog::id_not_found"
                            }
                        },
                        "response": "BAD"
                    }
          """
@exceptions
Scenario: Variable & value empty for id
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          When  request
          """
                      {
                        "data": {
                          "catalog": {
                            "": ""
                          }
                        }
                      }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                    {
                        "data": {
                            "exception": {
                                "code": "0027.000.001",
                                "detail": "data.catalog::id_not_found"
                            }
                        },
                        "response": "BAD"
                    }
          """
@exceptions
Scenario: catalog not is an object
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'Contract + Functional Testing'
          And   url baseUrlCatCryCurrencies
          And   def consulta = 'currencies'
          When  request
          """
                  {
                      "data": {
                          "catalog": [
                              {"id": "currencies"}
                          ]
                      }
                  }
          """
          And   method post
          Then  status 200
          And match response == 
          """
                    {
                        "data": {
                            "exception": {
                                "code": "0027.000.003",
                                "detail": "data::catalog_not_found"
                            }
                        },
                        "response": "BAD"
                    }
          """
@exceptions
Scenario: Different name in variable catalog

        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'Contract + Functional Testing'
        And   url baseUrlCatCryCurrencies
        And   def consulta = 'currencies'
        When  request
        """
                    {
                      "data": {
                        "catalogX": {
                          "id": '#(consulta)'
                        }
                      }
                    }
        """
        And   method post
        Then  status 200
        And match response == 
        """
                  {
                      "data": {
                          "exception": {
                              "code": "0027.000.003",
                              "detail": "data::catalog_not_found"
                          }
                      },
                      "response": "BAD"
                  }
        """


    @exceptions
Scenario: Variable & value´s empty for catalog object

        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'Contract + Functional Testing'
        And   url baseUrlCatCryCurrencies
        And   def consulta = ''
        When  request
        """
                    {
                      "data": {
                        "": {
                          "": '#(consulta)'
                        }
                      }
                    }
        """
        And   method post
        Then  status 200
        And match response == 
        """
                  {
                      "data": {
                          "exception": {
                              "code": "0027.000.003",
                              "detail": "data::catalog_not_found"
                          }
                      },
                      "response": "BAD"
                  }
        """


    @exceptions
Scenario: Variable & value´s empty for all Json

        Given header Content-Type = 'application/json'
        And   header Authorization = authorizationTest
        And   header Token = tokenTest
        And   header karate-name = 'Contract + Functional Testing'
        And   url baseUrlCatCryCurrencies
        And   def consulta = ''
        When  request
        """
                    {
                      "": {
                        "": {
                          "": '#(consulta)'
                        }
                      }
                    }
        """
        And   method post
        Then  status 200
        And match response == 
        """
                  {
                      "data": {
                          "exception": {
                              "code": "0027.000.000",
                              "detail": "JSONObject[\"data\"] not found."
                          }
                      },
                      "response": "BAD"
                  }
        """


    @exceptions
Scenario: Empty object
  Given header Content-Type = 'application/json'
  And   header Authorization = authorizationTest
  And   header Token = tokenTest
  And   header karate-name = 'Contract + Functional Testing'
  And   url baseUrlCatCryCurrencies
  And   def consulta = ''
  When  request
  """
              {
              }
  """
  And   method post
  Then  status 200
  And match response == 
  """
            {
                "data": {
                    "exception": {
                        "code": "0027.000.000",
                        "detail": "JSONObject[\"data\"] not found."
                    }
                },
                "response": "BAD"
            }
  """
        


@data
Scenario: Get currencie
        Given def id = db.getId('space_crypto_data', 'ct_currencies', 'key', 'BTC');
        And   print id

  Given header Content-Type = 'application/json'
  And   header Authorization = authorizationTest
  And   header Token = tokenTest
  And   header karate-name = 'CatalogCryptoCurrencies'
  And   url baseUrlCatCryCurrencies
  And   def consulta = 'currencies'
  And   def today = getDate()
  When  request
  """
              {
                "data": {
                  "catalog": {
                    "id": '#(consulta)'
                  }
                }
              }
  """
  And   method post
  Then  status 200