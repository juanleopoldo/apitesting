package alboapp.crypto.transfercommit;

import com.intuit.karate.junit5.Karate;

public class transferCommitRunner {


    //Run all Scenarios
    @Karate.Test
    Karate testSample() {
        return Karate.run("transfercommit").relativeTo(getClass());
    }
    
    //Run only test with tags
    @Karate.Test
    Karate testTags() {
        return Karate.run("transfercommit").tags("@first").relativeTo(getClass());
    }

    //Run test with tags and set properties
    @Karate.Test
    Karate testSystemProperty() {
        return Karate.run("classpath:alboApp/crypto/transfercommit/transfercommit.feature")
                .tags("@second")
                .karateEnv("sbx");
                // .systemProperty("foo", "bar")
    } 
    
    
}
