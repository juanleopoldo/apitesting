package alboapp.crypto.transfercreate;

import com.intuit.karate.junit5.Karate;

public class transferCreateRunner {


    //Run all Scenarios
    @Karate.Test
    Karate testSample() {
        return Karate.run("transferCreate").relativeTo(getClass());
    }
    
    //Run only test with tags
    @Karate.Test
    Karate testTags() {
        return Karate.run("transferCreate").tags("@first").relativeTo(getClass());
    }

    //Run test with tags and set properties
    @Karate.Test
    Karate testSystemProperty() {
        return Karate.run("classpath:alboApp/crypto/transferCreate/transferCreate.feature")
                .tags("@second")
                .karateEnv("sbx");
                // .systemProperty("foo", "bar")
    } 
    
    
}
