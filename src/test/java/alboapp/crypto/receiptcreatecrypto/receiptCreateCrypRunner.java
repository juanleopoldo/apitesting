package alboapp.crypto.receiptcreatecrypto;

import com.intuit.karate.junit5.Karate;

public class receiptCreateCrypRunner {


    //Run all Scenarios
    @Karate.Test
    Karate testSample() {
        return Karate.run("spacelistcrypto").relativeTo(getClass());
    }
    
    //Run only test with tags
    @Karate.Test
    Karate testTags() {
        return Karate.run("spacelistcrypto").tags("@first").relativeTo(getClass());
    }

    //Run test with tags and set properties
    @Karate.Test
    Karate testSystemProperty() {
        return Karate.run("classpath:alboApp/crypto/spacelistcrypto/spacelistcrypto.feature")
                .tags("@second")
                .karateEnv("sbx");
                // .systemProperty("foo", "bar")
    } 
    
    
}
