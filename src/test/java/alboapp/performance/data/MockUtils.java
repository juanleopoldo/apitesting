package alboapp.performance.data;


import com.intuit.karate.PerfContext;
import com.intuit.karate.Runner;


import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


public class MockUtils {
    

    // @SuppressWarnings("unchecked")
    
    private static final AtomicInteger counter = new AtomicInteger();
    
    private static final List<String> clabe = (List) Runner.runFeature("classpath:alboapp/performance/data/stpin.feature", null, false).get("clabe_uuid");

    public static String getNextStpinClabe() {
        return clabe.get(counter.getAndIncrement() % clabe.size());
    }

    private static final List<String> customer = (List) Runner.runFeature("classpath:alboapp/performance/data/customer.feature", null, false).get("customer_uuid");

    public static String getNextCustomer() {
        return customer.get(counter.getAndIncrement() % customer.size());
    }

    private static final List<String> wall = (List) Runner.runFeature("classpath:alboapp/performance/data/wallet.feature", null, false).get("wallet_uuid");

    public static String getNextWallet() {
        return wall.get(counter.getAndIncrement() % wall.size());
    }


    public static Map<String, Object> myRpc(Map<String, Object> map, PerfContext context) {
        long startTime = System.currentTimeMillis();
        // this is just an example, you can put any kind of code here
        int sleepTime = (Integer) map.get("sleep");
        try {
            Thread.sleep(sleepTime);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        long endTime = System.currentTimeMillis();
        // and here is where you send the performance data to the reporting engine
        context.capturePerfEvent("myRpc-" + sleepTime, startTime, endTime);
        return Collections.singletonMap("success", true);
    }

}