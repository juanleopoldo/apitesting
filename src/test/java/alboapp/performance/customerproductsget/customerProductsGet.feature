Feature: Load testing and logs per test

Scenario: Mirai home Screen

         * def customerTest = __gatling.Customer
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'customerProductsGet'
          And   url baseUrlCustomerProductsGet
          And   def num_max_tx = 3
          When  request
          """
                    {
                        "data": {
                            "customer_uuid": '#(customerTest)',
                            "num_max_tx": '#(num_max_tx)'
                        }
                    }
            """
          And   method post
          Then  status 200
          And   def resp = response.response
          And   def respuestaStatus = resp == "OK" ? 'Respuesta correcta':'Respuesta incorrecta'


#    And   def respuestaStatus = resp == 'OK' ? 'Response OK' : 'Response BAD'
#
         And   def responseApi = respuestaStatus == "Respuesta incorrecta" ? response:responseTime


          And   def textLogs = 'GetProductCustomer - ${Customer} - ${status} - ${Resp}'
          And   replace textLogs
            | token         | value                 |
            |${status}      | respuestaStatus       |
            |${Resp}        | responseApi           |
            |${Customer}    | customerTest          |
          And   print textLogs