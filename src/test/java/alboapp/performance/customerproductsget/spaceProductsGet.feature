Feature: Load testing and logs per test

Scenario: Mirai home Screen
          Given header Content-Type = 'application/json'
          And   header Authorization = authorizationTest
          And   header Token = tokenTest
          And   header karate-name = 'spaceProductsGet'
          And   url baseUrlSpaceProductsGet
          And   def num_max_tx = 2
          When  request
          """
                    {
                        "data": {
                            "customer_uuid": "16262811415260a196a4a-5eb4-4bd4-8206-607c62b07355",
                            "num_max_tx": '#(num_max_tx)'
                        }
                    }
            """
          And   method post
          Then  status 200
          And   print response
          # And   def resp = response.response
          # And   def respuestaStatus = resp == "OK" ? 'Respuesta correcta':'Respuesta incorrecta'

          # And def respTime = response.responseTime

          # And   def textLogs = 'GetCatalogCryCurrencies - ${status} - ${time} - BTC: ${btc} - COMP: ${comp} - ETH: ${eth} - ${Resptime}'
          # And   replace textLogs
          #   | token         | value                 |
          #   |${btc}         | BTC                   |
          #   |${comp}        | COMP                  |
          #   |${eth}         | ETH                   |
          #   |${status}      | respuestaStatus       |
          #   |${time}        | today                 |
          #   |${Resptime}    | responseTime          |
          # And   print textLogs