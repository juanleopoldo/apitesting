package alboapp.performance.ttk;

import org.apache.commons.codec.digest.HmacUtils;


public class checksum {

    static String key = "AB2PC1D417";

    public String checksumGenerator(String arg) {
        
        String st = HmacUtils.hmacSha1Hex(key, arg).toUpperCase();
        // System.out.println(st + "\n");
        return st;
    }

    
}
