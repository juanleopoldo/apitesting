Feature: DEDUCT TUTUKAKA

  Scenario: DEDUCT IN albo ACCOUNT

            #Data to generate Checksum        
            Given def Data = __gatling.Wallettk
            And def customer = get[0] Data
            And def space = get[1] Data
            And def wallet = get[2] Data
            And def wallType = get[3] Data
            And def terminal_id = "0036212719"
            And def method_name = "Deduct"
            And def ammount = "100"
            And def narrative = "loadtx" + __gatling.Number
            And def transaction_type = "00"
            ## Tx en MXN
            And def transaction_data = "0021562702810000002100412000000000200026045712049038400850025003EMV25110MasterCard2530451122540025500137033330420444442570555555"
            ## Tx en USD
            #And def transaction_data = "0021580172800000024100412000000000150010120000000020010260478290371200009234947004108000000010421500098002029699504800049038400500200052000850025004ECOM25110MasterCard25201025304219825801025903USD";
            And def random = function(max){ return Math.floor(Math.random() * max) }
            And def rastreoextract = random(99009230)
            And def transaction_id = rastreoextract + '10041'
            And def date = "20210823T14:40:10";
            #Generate Checksum
            And def chsm = method_name + terminal_id + wallet + ammount + narrative + transaction_type + transaction_data + transaction_id + date
            And def chekSum = Java.type('alboapp.performance.ttk.checksum')
            And def chkSm = new chekSum().checksumGenerator(chsm);
            And header Content-Type = 'application/xml'
            And header karate-name = 'Deduct - ttk'
            And url baseUrlGtwTtk
            When request
                """
                            <methodCall>
                                <methodName>#(method_name)</methodName>
                                <params>
                                    <param>
                                        <value>
                                            <string>#(terminal_id)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(wallet)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <int>#(ammount)</int>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(narrative)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(transaction_type)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(transaction_data)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(transaction_id)</string>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <dateTime.iso8601>#(date)</dateTime.iso8601>
                                        </value>
                                    </param>
                                    <param>
                                        <value>
                                            <string>#(chkSm)</string>
                                        </value>
                                    </param>
                                </params>
                            </methodCall>
                """
            And  method post
            Then status 200
            And  def resultadoDeduct = response
            And  string resultado = resultadoDeduct
            And  def respuesta = resultado
            And  def log = respuesta.contains('1') ? 'Cardeduct-OK':'NOK-Cardeduct'
            #Cardeduct-OK  -> Create target/DeductTTK.log
            #NOK-Cardeduct  -> Create target/NOKDeductTTK.log
            And  def textLogs = '${lg}|${cust}|${spc}|${key}|${type}|${name}|${amount}|${resp}'
            And  replace textLogs
            | token         | value                 |
            |${key}         | wallet                |
            |${type}        | wallType              |
            |${resp}        | respuesta             |
            |${lg}          | log                   |
            |${name}        | narrative             |
            |${amount}      | ammount               |
            |${spc}         | space                 |
            |${cust}        | customer              |
            And  print textLogs
            # And  match respuesta contains "1"