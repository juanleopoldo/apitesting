package performance

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import scala.concurrent.duration._
import alboapp.performance.data.MockUtils

class performanceTest extends Simulation {

  val protocol = karateProtocol()

  val feederStpin = Iterator.continually(Map("Clabe" -> MockUtils.getNextStpinClabe))
  val feederCustomer = Iterator.continually(Map("Customer" -> MockUtils.getNextCustomer))
  val feederWallet = Iterator.continually(Map("Wallettk" -> MockUtils.getNextWallet))
  val csvFeeder = csv("counter.csv").circular

    //protocol.nameResolver = (req, ctx) => req.getHeader("karate-name")


    

  val customerProductsGet = scenario("ProductGet").feed(feederCustomer).exec(karateFeature("classpath:alboapp/performance/customerproductsget/customerProductsGet.feature"))

  val stpin = scenario("Stpin").feed(feederStpin).exec(karateFeature("classpath:alboapp/performance/stpin/stpin.feature"))

  val deduct = scenario("Deduct").feed(csvFeeder).feed(feederWallet).exec(karateFeature("classpath:alboapp/performance/ttk/deduct.feature"))


  setUp(
    deduct.inject(atOnceUsers(3)).protocols(protocol)

  )

  // setUp(
  //   deduct.inject(
  //       atOnceUsers(1),
  //       nothingFor(4.seconds),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       constantUsersPerSec(50) during (1 second),
  //       nothingFor(5.seconds),   
  //       constantUsersPerSec(50) during (1 second)
  //   ).protocols(protocol)
  // )

//     setUp(
//     customerProductsGet.inject(
//         atOnceUsers(1),
//         nothingFor(3.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(1.second),
//         constantUsersPerSec(10) during (1 second),
//         nothingFor(5.seconds),
//         constantUsersPerSec(10) during (11 seconds)
//     ).protocols(protocol)
//   )

  //   setUp(
  //   customerProductsGet.inject(
  //       //1 request
  //       atOnceUsers(1),
  //       nothingFor(4.second),
  //        //10 seconds & 100 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //20 seconds & 200 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       nothingFor(5.seconds),
  //       //1 second & 10 request
  //       constantUsersPerSec(10) during (1 second)
  //   ).protocols(protocol)
  // )

  //     setUp(
  //   customerProductsGet.inject(
  //       //1 request
  //       atOnceUsers(1),
  //       nothingFor(4.second),
  //        //10 seconds & 100 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //20 seconds & 200 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //30 seconds & 300 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //40 seconds & 400 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //50 seconds & 500 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       //60 seconds & 600 request
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       constantUsersPerSec(10) during (1 second),
  //       nothingFor(5.seconds),
  //       //1 second & 10 request
  //       constantUsersPerSec(10) during (1 second)
  //   ).protocols(protocol)
  // )

  //     setUp(
  //   customerProductsGet.inject(
  //       //1 request
  //       atOnceUsers(1),
  //       nothingFor(4.second),
  //        //10 seconds & 100 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //20 seconds & 200 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //30 seconds & 300 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //40 seconds & 400 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //50 seconds & 500 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //60 seconds & 600 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //        //70 seconds & 100 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //80 seconds & 200 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //90 seconds & 300 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //100 seconds & 400 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //110 seconds & 500 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //120 seconds & 600 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //        //130 seconds & 100 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //140 seconds & 200 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //150 seconds & 300 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //160 seconds & 400 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //170 seconds & 500 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //180 seconds & 600 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //        //190 seconds & 100 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //200 seconds & 200 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //210 seconds & 300 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //220 seconds & 400 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //230 seconds & 500 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //240 seconds & 600 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //250 seconds & 100 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //260 seconds & 200 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //270 seconds & 300 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //280 seconds & 400 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //290 seconds & 500 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       //300 seconds & 600 request
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       constantUsersPerSec(20) during (1 second),
  //       nothingFor(5.seconds),
  //       //1 second & 20 request
  //       constantUsersPerSec(20) during (1 second)
  //   ).protocols(protocol)
  // )




}