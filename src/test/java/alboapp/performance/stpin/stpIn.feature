Feature: STPIN ABONO

  Scenario: STP IN
            * def clabe = __gatling.Clabe
            * def customer = get[0] clabe
            * def debit = get[1] clabe
            * def cuentaBeneficiario = get[3] clabe
            # * def cuentaBeneficiario = clabe
            * def random = function(max){ return Math.floor(Math.random() * max) }
            * def rastreoextract = random(990000)
            * def claveRastreo0 = 'PerTest' + rastreoextract
            * def rastreoextract1 = random(900000)
            * def claveRastreo1 = 'PerTest' + rastreoextract1
            * def montoTest = 0.25
            Given header Content-Type = 'application/xml'
            And   header karate-name = 'Perfilador - Stpin'
            And   url baseUrlPerStpIn

        #    And   header karate-name = 'Stpin'
        #    And   url baseUrlStpIn
            When  request
                """
                            <abono>
                                <clave>#(claveRastreo0)</clave>
                                <claveRastreo>#(claveRastreo1)</claveRastreo>
                                <nombreOrdenante>Juan</nombreOrdenante>
                                <nombreBeneficiario>Juan Enciso</nombreBeneficiario>
                                <cuentaBeneficiario>#(cuentaBeneficiario)</cuentaBeneficiario>
                                <conceptoPago>Test</conceptoPago>
                                <monto>#(montoTest)</monto>
                                <cuentaOrdenante>1234567890</cuentaOrdenante>
                                <referenciaNumerica>19066df6</referenciaNumerica>
                                <fechaOperacion>20200605</fechaOperacion>
                                <institucionOrdenante clave="40012"/>
                                <institucionBeneficiaria clave="90646"/>
                                <tipoCuentaOrdenante>10</tipoCuentaOrdenante>
                                <rfcCurpOrdenante>EICJ8509279T1</rfcCurpOrdenante>
                                <tipoCuentaBeneficiario>40</tipoCuentaBeneficiario>
                                <rfcCurpBeneficiario>EICJ8509279T1</rfcCurpBeneficiario>
                                <tipoPago clave="1"/>
                                <tipoOperacion clave="0"/>
                                <empresa>ALBO</empresa>
                            </abono>

                """
            And  method post

            Then status 200

            And  def resultadoStpIn = response

            And  string resultado = resultadoStpIn

            And  def respuesta = resultado

        #	  And def respuestaStatus = resp == "status>accepted</status>" ? 'Respuesta correcta':'Respuesta incorrecta'

            And def textLogs = 'Stp-in,${resp},${key}'

            And replace textLogs
            | token         | value                 |
            |${key}         | cuentaBeneficiario    |
            |${resp}        | respuesta             |


            And print textLogs