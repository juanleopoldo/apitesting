function fn() {

  karate.configure('charset', null);
  karate.configure('logPrettyRequest', true);
  karate.configure('logPrettyResponse', true);
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  
  if (!env) {
    env = 'sbx';
  }

  var config = {
    env: env,
    myVarName: 'someValue'
  }

  if (env == 'dev') {
    // customize
    // e.g. config.foo = 'bar';
  } else if (env == 'qa') {
    // customize
    config.baseUrlCatCryCurrencies = 'https://albo-mirai-app.qa-albo.tech/space-crypto/catalogList'

    config.baseUrlSpaceCryList = 'https://albo-mirai-app.qa-albo.tech/space-crypto/spaceList'

    config.baseUrlTransferCreateCrypto = 'https://albo-mirai-app.qa-albo.tech/space-crypto/transferCreate'

    config.baseUrlMoneyOut = 'https://albo-mirai-app.sbx-albo.tech/fortknox/moneyoutRequest'

    config.baseUrlCustomerProductsGet = 'https://albo-mirai-app.qa-albo.tech/customer/customerProductsGet'

    config.baseUrlSpaceProductsGet = 'https://albo-mirai-app.qa-albo.tech/customer/spacesProductsGet'

    config.authorizationTest = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhIjoiYWxiby5hcHAuYW5kcm9pZEAxNy4wIiwiZCI6Ik1vdG9yb2xhIDggUGx1cyIsImFlIjoiQXBwIEhvbWUgUmVmcmVzaCIsImRpIjoiTU9UT1VVSURYRFhEIiwicHYiOiIxMC4yLjEiLCJnbGF0IjoiMTguMjU3MjQyIiwiZG0iOiJbWFhYWFhYWCxYWFhYWFhYXSIsImFtIjoiaG9tZS5iYWxhbmNlIiwicCI6IkFuZHJvaWQiLCJzIjoic2RrX21pcmFpLW1vYmlsZUAxLjAuMzMiLCJnbG9uIjoiLTEwNi41MjA2MzgiLCJ1IjoiMTYxNDk1ODg3MTgyMDRmMmM2YjFmLTBhZDAtNGZjOS04ODhiLTJjZWEyYmJjMDY5MyIsImlhdCI6MTUxNjIzOTAyMn0.qQpaVqgqAUyE_VtNsq46WJLlJPITmbQCHDTBwb8k'

    config.tokenTest = 'Qsz/uXpUffRHbfUlZqN/xqZb6Ao='


  } else if(env == 'sbx'){

    config.baseUrlCatCryCurrencies = 'https://albo-mirai-app.sbx-albo.tech/space-crypto/catalogList'

    config.baseUrlSpaceCryList = 'https://albo-mirai-app.sbx-albo.tech/space-crypto/spaceList'

    config.baseUrlTransferCreateCrypto = 'https://albo-mirai-app.sbx-albo.tech/space-crypto/transferCreate'

    config.baseUrlSpaceListTxCrypto = 'https://albo-mirai-app.sbx-albo.tech/space-crypto/spaceListTx'

    config.baseUrlMoneyOut = 'https://albo-mirai-app.sbx-albo.tech/fortknox/moneyoutRequest'

    config.baseUrlCustomerProductsGet = 'https://albo-mirai-app.sbx-albo.tech/customer/customerProductsGet'

    config.baseUrlSpaceProductsGet = 'https://albo-mirai-app.sbx-albo.tech/customer/spacesProductsGet'
    
    config.baseUrlPerStpIn = 'http://profiler-dot-albo-mx-labs.appspot.com/profiler/stp-in'

    config.baseUrlGtwTtk = 'https://tutuka-gateway.sbx-albo.tech/gateway/tutuka@1.0'

    config.authorizationTest = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzM4NCJ9.eyJhIjoiMS4xNy4wIiwiYWUiOiJTdGFydCBpbmdyZXNzIiwiZCI6IlNpbXVsYXRvciBpUGhvbmUxMiw4IiwiZGkiOiIxQkFEMzBERS1COEQ5LTRBMTctQTlFNi1BMTA3QUYzMUQ0MTUtdGVzdCIsInB2IjoiMTMuMi4zIiwiZ2xhdCI6MCwiZG0iOiIwMjowMDowMDowMDowMDowMCIsImFtIjoiaW5ncmVzcyIsInAiOiJpb3MiLCJzIjoibWlyYWktaW9zQDEuMC4zIiwiZ2xvbiI6MCwidSI6IjE2MDIwOTczMTg4NDZiOWQwODUyMy1lZTlkLTQwNzItOGMxNC1lYTdmOTNmZjYxZGIiLCJpYXQiOjB9.fX2EycmHN4ji3hcxuopu3SREi_griLpuSjfFDJC507vpz-9y0eLVSn1ovGTLUj80eQarI2S_4tbr3HgPnfm9A09kBepr0sSda5RaRurC7zivPS5cX0z1nPobp533a7Y-tq705aA3iVwl_tAgTrh7pPWIRN1kL4VzDIXmtA7N2ZMifFlsc0O4BHFZ89IwttY_omoDDXXEVJkvj9Ehd3Tn-X0AfDBV4_M-1wJqmZg37u3V9DgVW93KIL1AMq0xBhsLwmVRy-pgsRIdeH3E4rCaTZLw3eO-xQ2CrlBypI_s09EgnQNReqpXJ9fNMwtOl276vT-tM58doFTT99yI2AynDglYsXVWHuYPja1HgFASLkM2_QE97VzWdewR1g4sOzIvPYfaA2-WmZ5edgT5sMKMqVA5vi7kabIql0Kk6ka65mqTw6IFHRvvR2xqRsr1cz4UVPavrF7-2hFfOeFkCYd-FOQpXbzMQy1Mw9tUCC5JAItYGj9V1E_OTpnz7J61mWHq'

    config.tokenTest = 'hCgQ0OBqwKo5pA/0Srbf+lYyGBA='

  }
  return config;
}