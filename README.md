																						  QA Automation
																					Test Automation Made Simple

																	 :::::::::ApiTesting with Karate DSL + Gatling:::::::::


"Getting Started"

If you are a Java Rocker - Karate requires at least Java 8 and then either Maven, Gradle, Eclipse or IntelliJ to be installed. Note that Karate works fine on OpenJDK.

If you are new to programming or test-automation, Visual Studio Code can be used for Java (or Maven) projects as well. One reason to use it is the excellent debug support that we have for Karate.


"Install, this program´s":

A)Java 8 o superior

In the terminal, check the instalation with: 

1.- java -version

Example: java version "11.0.4

2.- echo $JAVA_HOME

Example:/Library/Java/JavaVirtualMachines/jdk-11.0.4.jdk/Contents/Home

B)Maven

In the terminal, check the instalation with: 

1. -mvn -v

C)Git

In the terminal, check the instalation with: 

1.- git --version

Example:git version 2.21.0 (Apple Git-122)

D)Postman

E)Visual Studio Code and extensions:
	1)Cucumber (Gherkin) Full support
	2)Karate Runner
	3)Java Extension Pack
	4)Java Test Runner (uninstall and Reload)
	5)Material Icon Theme
	6)Open in default Browser
	
	https://code.visualstudio.com/
	
"Naming Conventions"

Since these are tests and not production Java code, you don't need to be bound by the com.mycompany.foo.bar convention and the un-necessary explosion of sub-folders that ensues. We suggest that you have a folder hierarchy only one or two levels deep - where the folder names clearly identify which 'resource', 'entity' or API is the web-service under test.

Directory structure:

src/test/java
    |
    +-- karate-config.js
    +-- logback-test.xml
    +-- some-reusable.feature
    +-- some-classpath-function.js
    +-- some-classpath-payload.json
    |
    \-- alboapp
        |
        +-- alboAppParallelTest.java
        |
        +-- crypto
        |   |
        |   +--cataloglist
        |   |   |
        |   |   +-- catalgoCurrencies.feature
        |   |   +-- currencie.json
        |   |   \-- catalgoCurrenciesRunner.java
        |   |
        |   +--transfercrete
        |       |    
        |       +-- transferCreate.feature
        |       +-- customers.json
        |       \-- transferCreateRunner.java
        |
        \-- debit
            |
            +-- debit.feature
            +-- spaceDebit.json
            +-- some-helper-function.js
            \-- debitRunner.java

Note that the mvn test command only runs test classes that follow the *Test.java naming convention by default. But you can choose a single test to run like this:

mvn test -Dtest=CatsRunner

"karate.options"

When your Java test "runner" is linked to multiple feature files, which will be the case when you use the recommended parallel runner, you can narrow down your scope to a single feature, scenario or directory via the command-line, useful in dev-mode. Note how even tags to exclude (or include) can be specified:

mvn test "-Dkarate.options=--tags ~@ignore classpath:alboapp/crypyo/catalogCurrencies.feature" -Dtest=alboAppParallelTest


"Parallel Execution"

Karate can run tests in parallel, and dramatically cut down execution time. This is a 'core' feature and does not depend on JUnit, Maven or Gradle.

You can easily "choose" features and tags to run and compose test-suites in a very flexible manner.
You can use the returned Results object to check if any scenarios failed, and to even summarize the errors
JUnit XML reports can be generated in the "reportDir" path you specify, and you can easily configure your CI to look for these files after a build (for e.g. in **/*.xml or **/karate-reports/*.xml). Note that you have to call the outputJunitXml(true) method on the Runner "builder".
Cucumber JSON reports can be generated, except that the extension will be .json instead of .xml. Note that you have to call the outputCucumberJson(true) method on the Runner "builder".

In the file alboAppParallelTest.java you can set the parallel Execution:

    @Test
    public void testParallel() {
        System.setProperty("karate.env", "sbx"); // ensure reset if other tests (e.g. mock) had set env in CI
        Results results = Runner.path("classpath:alboapp")
                .outputCucumberJson(true)
                .tags("~@ignore").parallel(5);
        generateReport(results.getReportDir());
        assertEquals(0, results.getFailCount(), results.getErrorMessages());        
    }
	

	
	
